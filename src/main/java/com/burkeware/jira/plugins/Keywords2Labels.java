package com.burkeware.jira.plugins;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ofbiz.core.entity.GenericEntityException;

import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.issue.label.LabelManager;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;

public class Keywords2Labels extends HttpServlet {

	private static final String CUSTOMFIELD_PARAM = "customField";
	private static final String CUSTOMFIELD_POST_PARAM = "_customField";
	private static final String KEYWORDS2LABEL_SECRET_PARAM = "secret";
	private static final String KEYWORDS2LABEL_SECRET = "keywords2label-powers-activate!";

	private ProjectManager projectManager;
	private IssueManager issueManager;
	private CustomFieldManager customFieldManager;
	private LabelManager labelManager;

	public Keywords2Labels(ProjectManager projectManager, IssueManager issueManager, CustomFieldManager customFieldManager,
			LabelManager labelManager) {
		this.projectManager = projectManager;
		this.issueManager = issueManager;
		this.customFieldManager = customFieldManager;
		this.labelManager = labelManager;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ServletOutputStream out = resp.getOutputStream();
		String customFieldName = req.getParameter(CUSTOMFIELD_PARAM);
		String maxIssuesParam = req.getParameter("max");
		int maxIssues = -1;
		try {
			if (maxIssuesParam != null)
				maxIssues = Integer.parseInt(maxIssuesParam);
		} catch (NumberFormatException e) {
			maxIssues = -1;
		}
		if (customFieldName == null || customFieldName.length() < 1 || maxIssues == -1) {
			out.print("<form method=get>Name of custom field with labels:");
			out.print("<input name='" + CUSTOMFIELD_PARAM + "' type=text size=40 /><br />");
			out.print("Maximum number of issues to process (0 = no limit):");
			out.print("<input name='max' type=text size=5 value='10' />");
			out.print("<input type=submit /></form>");
		} else {
			out.println("<div style='text-align:center;padding:1em;margin:2em;border:thick solid red'>"
					+ "<b>Test run only.</b> <em>No changes are being made</em></div>");
			moveKeywords(out, customFieldName, maxIssues, false);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ServletOutputStream out = resp.getOutputStream();
		String customFieldName = req.getParameter(CUSTOMFIELD_POST_PARAM);
		String maxIssuesParam = req.getParameter("max");
		int maxIssues = -1;
		try {
			if (maxIssuesParam != null)
				maxIssues = Integer.parseInt(maxIssuesParam);
		} catch (NumberFormatException e) {
			maxIssues = -1;
		}
		String secret = req.getParameter(KEYWORDS2LABEL_SECRET_PARAM);
		if (!KEYWORDS2LABEL_SECRET.equals(secret)) {
			resp.sendError(404); // post without secret, act dumb
		} else if (customFieldName == null || customFieldName.length() < 1) {
			resp.sendError(400); // missing custom field name
		} else if (maxIssues == -1) {
			resp.sendError(400); // missing maximum issue count
		} else {
			moveKeywords(out, customFieldName, maxIssues, true);
		}
	}

	private void moveKeywords(ServletOutputStream out, String customFieldName, int maxIssues, boolean makeChanges)
			throws IOException {
		int totalIssuesChanged = 0;
		PROJECT_LOOP: for (Project project : projectManager.getProjectObjects()) {
			out.println("<b>" + project.getName() + " (" + project.getKey() + ")</b>: ");
			int numIssuesProcessed = 0;
			int numIssuesChanged = 0;
			int numKeywordsMoved = 0;
			try {
				ISSUE_LOOP: for (Long issueId : issueManager.getIssueIdsForProject(project.getId())) {
					try {
						MutableIssue issue = issueManager.getIssueObject(issueId);
						numIssuesProcessed++;
						for (CustomField customField : customFieldManager.getCustomFieldObjects(issue)) {
							if (customFieldName.equals(customField.getName())) {
								CustomFieldType type = customField.getCustomFieldType();
								if ("Labels".equals(type.getName())) {
									@SuppressWarnings("unchecked")
									Set<Label> customFieldValues = (Set<Label>) customField.getValue(issue);
									if (customFieldValues != null && customFieldValues.size() > 0) {
										numIssuesChanged++;
										totalIssuesChanged++;
										for (Label keyword : customFieldValues) {
											if (makeChanges)
												labelManager.addLabel(null, issueId, keyword.getLabel(), false);
											numKeywordsMoved++;
										}
										if (makeChanges) {
											customField.removeValueFromIssueObject(issue);
											IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
											customField
													.updateValue(null, issue,
															new ModifiedValue(issue.getCustomFieldValue(customField),
																	new HashSet<Label>()), changeHolder);
										}
									}
								}
							}
						}
					} catch (DataAccessException e) {
						e.printStackTrace(System.err);
					}
					if (maxIssues > 0 && totalIssuesChanged >= maxIssues)
						break ISSUE_LOOP;
				}
			} catch (GenericEntityException e) {
				e.printStackTrace(System.err);
			}
			if (numIssuesProcessed == 0)
				out.println(" no issues processed.<br/>");
			else if (numIssuesChanged == 0)
				out.println(" no changes across " + numIssuesProcessed + " issue(s)<br/>");
			else
				out.println(numKeywordsMoved + " keyword(s) moved, affecting " + numIssuesChanged + " of " + numIssuesProcessed
						+ " issue(s)<br/>");
			if (maxIssues > 0 && totalIssuesChanged >= maxIssues)
				break PROJECT_LOOP;
		}
		if (!makeChanges) {
			out.println("<hr />");
			out.println("<form method=post><input type=hidden name='" + CUSTOMFIELD_POST_PARAM + "' value='" + customFieldName
					+ "' />");
			out.println("<input type=hidden name='" + KEYWORDS2LABEL_SECRET_PARAM + "' value='" + KEYWORDS2LABEL_SECRET + "' />");
			out.println("<input type=submit value='Run again and make changes' /></form>");
		}
	}
}
